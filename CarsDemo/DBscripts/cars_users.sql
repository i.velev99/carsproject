INSERT INTO cars.users (id, first_name, last_name, credentials_id) VALUES (1, 'Ivan', 'Ivanov', 'ivan');
INSERT INTO cars.users (id, first_name, last_name, credentials_id) VALUES (2, 'Georgi', 'Petrov', 'georgi');
INSERT INTO cars.users (id, first_name, last_name, credentials_id) VALUES (3, 'Petar', 'Ivanov', 'petar');
INSERT INTO cars.users (id, first_name, last_name, credentials_id) VALUES (4, 'Dimitar', 'Simeonov', 'dimitar');
INSERT INTO cars.users (id, first_name, last_name, credentials_id) VALUES (5, 'Simeon', 'Georgiev', 'simeon');
INSERT INTO cars.users (id, first_name, last_name, credentials_id) VALUES (6, 'Viktor', 'Kuzmanov', 'viktor');
INSERT INTO cars.users (id, first_name, last_name, credentials_id) VALUES (7, 'Atanas', 'Petrov', 'atanas');