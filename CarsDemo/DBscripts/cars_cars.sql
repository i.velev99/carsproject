INSERT INTO cars.cars (id, brand_id, model_id, user_id, licence_plate, cubature, color_id, horse_powers) VALUES (1, 1, 2, 1, '??9910??', 2000, 2, 140);
INSERT INTO cars.cars (id, brand_id, model_id, user_id, licence_plate, cubature, color_id, horse_powers) VALUES (3, 1, 1, 3, 'AB1234GA', 3000, 1, 230);
INSERT INTO cars.cars (id, brand_id, model_id, user_id, licence_plate, cubature, color_id, horse_powers) VALUES (4, 2, 4, 2, 'AS3461AG', 2500, 1, 160);
INSERT INTO cars.cars (id, brand_id, model_id, user_id, licence_plate, cubature, color_id, horse_powers) VALUES (5, 3, 8, 4, 'QS234AD', 1234, 4, 100);
INSERT INTO cars.cars (id, brand_id, model_id, user_id, licence_plate, cubature, color_id, horse_powers) VALUES (6, 4, 12, 5, 'ASD1234', 2345, 3, 123);