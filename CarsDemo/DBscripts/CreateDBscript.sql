create table brands
(
    id    int auto_increment
        primary key,
    brand varchar(100) not null,
    constraint brands_brand_uindex
        unique (brand)
);

create table colors
(
    id    int auto_increment
        primary key,
    color varchar(100) not null,
    constraint colors_color_uindex
        unique (color)
);

create table models
(
    id    int auto_increment
        primary key,
    model varchar(300) not null,
    constraint models_model_uindex
        unique (model)
);

create table user_credentials
(
    username varchar(45) not null
        primary key
);

create table users
(
    id             int auto_increment
        primary key,
    first_name     varchar(20) not null,
    last_name      varchar(20) not null,
    credentials_id varchar(50) not null,
    constraint users_user_credentials_username_fk
        foreign key (credentials_id) references user_credentials (username)
);

create table cars
(
    id            int auto_increment
        primary key,
    brand_id      int         not null,
    model_id      int         not null,
    user_id       int         not null,
    licence_plate varchar(20) not null,
    cubature      int null,
    color_id      int         not null,
    horse_powers  int         not null,
    constraint cars_licence_plate_uindex
        unique (licence_plate),
    constraint cars_brands_id_fk
        foreign key (brand_id) references brands (id),
    constraint cars_colors_id_fk
        foreign key (color_id) references colors (id),
    constraint cars_models_id_fk
        foreign key (model_id) references models (id),
    constraint cars_users_id_fk
        foreign key (user_id) references users (id)
);


