package com.example.carsdemo;

import com.example.carsdemo.models.*;

public class Helper {

    public static UserCredentials createMockUserCredentials() {
        return new UserCredentials("ivan");
    }

    public static User createMockUser() {
        return new User(1, "Ivan", "Ivanov", createMockUserCredentials());
    }

    public static Brand createMockBrand() {
        return new Brand(1, "audi");
    }

    public static Model createMockModel() {
        return new Model(1, "a4");
    }

    public static Color createMockColor() {
        return new Color(1, "black");
    }

    public static Car createMockCar() {
        return new Car(1, createMockUser(), createMockBrand(), createMockModel(), createMockColor(), "asdfg", 2000, 170);
    }

}
