package com.example.carsdemo.services;

import com.example.carsdemo.Helper;
import com.example.carsdemo.exceptions.DuplicateEntityException;
import com.example.carsdemo.exceptions.EntityNotFoundException;
import com.example.carsdemo.models.Car;
import com.example.carsdemo.models.filterparameters.CarFilterParameters;
import com.example.carsdemo.repositories.CarRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class CarServiceTests {

    @Mock
    private CarRepository carRepository;

    @InjectMocks
    private CarServiceImpl carService;

    @Test
    public void filterCars_Should_CallRepository_When_BrandIdParameterOnly() {
        var car = Helper.createMockCar();

        CarFilterParameters cfp = new CarFilterParameters(Optional.of(1), Optional.empty());

        carService.filter(cfp);

        Mockito.verify(carRepository, Mockito.times(1))
                .filter(cfp);
    }

    @Test
    public void filterUsers_Should_CallRepository_When_ModelIdParameterOnly() {
        var car = Helper.createMockCar();

        CarFilterParameters cfp = new CarFilterParameters(Optional.empty(), Optional.of(1));

        carService.filter(cfp);

        Mockito.verify(carRepository, Mockito.times(1))
                .filter(cfp);
    }

    @Test
    public void filterUsers_Should_CallRepository_When_NoParameters() {
        var car = Helper.createMockCar();

        CarFilterParameters cfp = new CarFilterParameters(Optional.empty(), Optional.empty());

        carService.filter(cfp);

        Mockito.verify(carRepository, Mockito.times(1))
                .filter(cfp);
    }

    @Test
    public void getById_Should_ReturnCar_When_MatchExists() {

        //Arrange
        Mockito.when(carRepository.getById(1))
                .thenReturn(Helper.createMockCar());

        //Act
        Car result = carService.getById(1);

        //Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("audi", result.getBrand().getBrand());
        Assertions.assertEquals("a4", result.getModel().getModel());
        Assertions.assertEquals("ivan", result.getOwner().getUserCredentials().getUsername());
    }

    @Test
    public void create_Should_Throw_When_CarWithSameLicencePlateExists() {
        //Arrange
        Car car = Helper.createMockCar();

        Mockito.when(carRepository.getByLicencePlate(car.getLicencePlate()))
                .thenReturn(car);

        Car secondCar = Helper.createMockCar();
        secondCar.setId(2);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> carService.create(secondCar));
    }

    @Test
    public void create_Should_CallRepository_when_LicencePlateUnique() {
        //Arrange
        Car car = Helper.createMockCar();

        Mockito.when(carRepository.getByLicencePlate(car.getLicencePlate()))
                .thenThrow(EntityNotFoundException.class);

        carService.create(car);

        Mockito.verify(carRepository, Mockito.times(1))
                .create(car);
    }

    @Test
    public void update_Should_Throw_When_CarWithSameLicencePlateExists() {
        //Arrange
        Car car = Helper.createMockCar();

        Mockito.when(carRepository.getByLicencePlate(car.getLicencePlate()))
                .thenReturn(car);

        Car secondCar = Helper.createMockCar();
        secondCar.setId(2);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> carService.update(secondCar));
    }

    @Test
    public void update_Should_CallRepository_when_LicencePlateUnique() {
        //Arrange
        Car car = Helper.createMockCar();

        Mockito.when(carRepository.getByLicencePlate(car.getLicencePlate()))
                .thenThrow(EntityNotFoundException.class);

        carService.update(car);

        Mockito.verify(carRepository, Mockito.times(1))
                .update(car);
    }

    @Test
    public void delete_Should_CallRepository() {

        var car = Helper.createMockCar();

        carService.delete(car);

        Mockito.verify(carRepository, Mockito.times(1))
                .delete(car);
    }

}
