package com.example.carsdemo.services;

import com.example.carsdemo.Helper;
import com.example.carsdemo.exceptions.DuplicateEntityException;
import com.example.carsdemo.exceptions.EntityNotFoundException;
import com.example.carsdemo.models.User;
import com.example.carsdemo.repositories.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;

    @Test
    public void getAll_Should_CallRepository() {
        userService.getAll();
        Mockito.verify(userRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getById_Should_ReturnUser_When_MatchExists() {

        //Arrange
        Mockito.when(userRepository.getById(1))
                .thenReturn(Helper.createMockUser());

        //Act
        User result = userService.getById(1);

        //Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("Ivan", result.getFirstName());
        Assertions.assertEquals("Ivanov", result.getLastName());
        Assertions.assertEquals("ivan", result.getUserCredentials().getUsername());
    }

    @Test
    public void getByUsername_Should_ReturnUser_When_MatchExists() {

        //Arrange
        Mockito.when(userRepository.getByUsername("ivan"))
                .thenReturn(Helper.createMockUser());

        //Act
        User result = userService.getByUsername("ivan");

        //Assert
        Assertions.assertEquals(1, result.getId());
        Assertions.assertEquals("Ivan", result.getFirstName());
        Assertions.assertEquals("Ivanov", result.getLastName());
        Assertions.assertEquals("ivan", result.getUserCredentials().getUsername());
    }

    @Test
    public void create_Should_Throw_When_UserWithSameUsernameExists() {
        //Arrange
        User user = Helper.createMockUser();

        Mockito.when(userRepository.getByUsername(user.getUserCredentials().getUsername()))
                .thenReturn(user);

        User secondUser = Helper.createMockUser();
        secondUser.setId(2);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.create(secondUser));
    }

    @Test
    public void create_Should_CallRepository_when_UserCredentialsUnique() {

        var user = Helper.createMockUser();

        Mockito.when(userRepository.getByUsername(user.getUserCredentials().getUsername()))
                .thenThrow(EntityNotFoundException.class);

        userService.create(user);

        Mockito.verify(userRepository, Mockito.times(1))
                .create(user);
    }

    @Test
    public void update_Should_Throw_When_UserWithSameUsernameExists() {
        //Arrange
        User user = Helper.createMockUser();

        User secondUser = Helper.createMockUser();
        secondUser.setId(2);

        Mockito.when(userRepository.getByUsername(user.getUserCredentials().getUsername()))
                .thenReturn(user);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> userService.update(secondUser));
    }

    @Test
    public void update_Should_CallRepository_when_UserCredentialsUnique() {

        var user = Helper.createMockUser();

        Mockito.when(userRepository.getByUsername(user.getUserCredentials().getUsername()))
                .thenThrow(EntityNotFoundException.class);


        userService.update(user);

        Mockito.verify(userRepository, Mockito.times(1))
                .update(user);
    }

    @Test
    public void delete_Should_CallRepository_when_UserExists() {

        var user = Helper.createMockUser();

        userService.delete(user);

        Mockito.verify(userRepository, Mockito.times(1))
                .delete(user);
    }

}
