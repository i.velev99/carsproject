package com.example.carsdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarsDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarsDemoApplication.class, args);
    }

}
