package com.example.carsdemo.models;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;

@Entity
@Table(name = "cars")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @ManyToOne
    @Fetch(FetchMode.SELECT)
    @JoinColumn(name = "user_id")
    private User owner;

    @ManyToOne
    @Fetch(FetchMode.SELECT)
    @JoinColumn(name = "brand_id")
    private Brand brand;

    @ManyToOne
    @Fetch(FetchMode.SELECT)
    @JoinColumn(name = "model_id")
    private Model model;

    @ManyToOne
    @Fetch(FetchMode.SELECT)
    @JoinColumn(name = "color_id")
    private Color color;

    @Column(name = "licence_plate")
    private String licencePlate;

    @Column(name = "cubature")
    private int cubature;

    @Column(name = "horse_powers")
    private int horsePowers;

    public Car() {
    }

    public Car(int id, User owner, Brand brand, Model model, Color color, String licencePlate, int cubature, int horsePowers) {
        this.id = id;
        this.owner = owner;
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.licencePlate = licencePlate;
        this.cubature = cubature;
        this.horsePowers = horsePowers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public int getCubature() {
        return cubature;
    }

    public void setCubature(int cubature) {
        this.cubature = cubature;
    }

    public int getHorsePowers() {
        return horsePowers;
    }

    public void setHorsePowers(int horsePowers) {
        this.horsePowers = horsePowers;
    }
}
