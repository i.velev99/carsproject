package com.example.carsdemo.models.dto;

import javax.validation.constraints.NotNull;

public class CarDto {

    @NotNull
    private int brandId;

    @NotNull
    private int modelId;

    @NotNull
    private int userId;

    @NotNull
    private String licencePlate;

    @NotNull
    private int cubature;

    @NotNull
    private int colorId;

    @NotNull
    private int horsePowers;

    public CarDto() {
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public int getCubature() {
        return cubature;
    }

    public void setCubature(int cubature) {
        this.cubature = cubature;
    }

    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    public int getHorsePowers() {
        return horsePowers;
    }

    public void setHorsePowers(int horsePowers) {
        this.horsePowers = horsePowers;
    }
}
