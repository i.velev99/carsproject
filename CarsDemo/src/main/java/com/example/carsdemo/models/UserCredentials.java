package com.example.carsdemo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "user_credentials")
public class UserCredentials {

    @Id
    @Column(name = "username")
    @NotNull
    private String username;

    public UserCredentials() {
    }

    public UserCredentials(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}

