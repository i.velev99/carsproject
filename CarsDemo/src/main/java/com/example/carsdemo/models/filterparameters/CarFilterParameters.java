package com.example.carsdemo.models.filterparameters;

import java.util.Optional;

public class CarFilterParameters {

    private Optional<Integer> brandId;
    private Optional<Integer> modelId;

    public CarFilterParameters() {
    }

    public CarFilterParameters(Optional<Integer> brandId, Optional<Integer> modelId) {
        this.brandId = brandId;
        this.modelId = modelId;
    }

    public Optional<Integer> getBrandId() {
        return brandId;
    }

    public void setBrandId(Optional<Integer> brandId) {
        this.brandId = brandId;
    }

    public Optional<Integer> getModelId() {
        return modelId;
    }

    public void setModelId(Optional<Integer> modelId) {
        this.modelId = modelId;
    }
}
