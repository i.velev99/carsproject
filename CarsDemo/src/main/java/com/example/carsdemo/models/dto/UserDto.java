package com.example.carsdemo.models.dto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserDto {

    @NotNull(message = "First name should not be null")
    @Size(min = 2, max = 20, message = "First name should be between 2 and 20")
    private String firstName;

    @NotNull(message = "Last name should not be null")
    @Size(min = 2, max = 20, message = "Last name should be between 2 and 20")
    private String lastName;

    @Valid
    private UserCredentialsDto userCredentialsDto;

    public UserDto() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public UserCredentialsDto getUserCredentialsDto() {
        return userCredentialsDto;
    }

    public void setUserCredentialsDto(UserCredentialsDto userCredentialsDto) {
        this.userCredentialsDto = userCredentialsDto;
    }
}
