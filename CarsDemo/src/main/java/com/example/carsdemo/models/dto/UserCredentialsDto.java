package com.example.carsdemo.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class UserCredentialsDto {

    @NotNull(message = "Username should not be null")
    @Size(min = 4, max = 20, message = "Username should be between 4 and 20")
    private String username;

    public UserCredentialsDto() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
