package com.example.carsdemo.controllers.rest;

import com.example.carsdemo.controllers.ModelMapper;
import com.example.carsdemo.exceptions.DuplicateEntityException;
import com.example.carsdemo.exceptions.EntityNotFoundException;
import com.example.carsdemo.models.Car;
import com.example.carsdemo.models.dto.CarDto;
import com.example.carsdemo.models.filterparameters.CarFilterParameters;
import com.example.carsdemo.services.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/cars")
public class CarController {

    private final CarService carService;
    private final ModelMapper modelMapper;

    @Autowired
    public CarController(CarService carService,
                         ModelMapper modelMapper) {
        this.carService = carService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    public List<Car> filter(@RequestParam(required = false) Optional<Integer> brandId,
                            @RequestParam(required = false) Optional<Integer> modelId) {
        CarFilterParameters filterParameters = modelMapper.fromParametersList(brandId, modelId);
        return carService.filter(filterParameters);
    }

    @GetMapping("/{id}")
    public Car getById(@PathVariable int id) {
        try {
            return carService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Car create(@Valid @RequestBody CarDto carDto) {
        try {
            Car car = modelMapper.fromDto(carDto);
            carService.create(car);
            return car;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Car update(@PathVariable int id,
                       @Valid @RequestBody CarDto carDto) {
        try {
            Car car = modelMapper.fromDto(carDto, id);
            carService.update(car);
            return car;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public Car delete(@PathVariable int id) {
        try {
            Car car = getById(id);
            carService.delete(car);
            return car;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
