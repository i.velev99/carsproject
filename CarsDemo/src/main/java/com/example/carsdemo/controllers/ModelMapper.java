package com.example.carsdemo.controllers;

import com.example.carsdemo.models.Car;
import com.example.carsdemo.models.User;
import com.example.carsdemo.models.UserCredentials;
import com.example.carsdemo.models.dto.CarDto;
import com.example.carsdemo.models.dto.UserCredentialsDto;
import com.example.carsdemo.models.dto.UserDto;
import com.example.carsdemo.models.filterparameters.CarFilterParameters;
import com.example.carsdemo.repositories.*;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class ModelMapper {

    private final UserRepository userRepository;
    private final BrandRepository brandRepository;
    private final ColorRepository colorRepository;
    private final ModelRepository modelRepository;
    private final CarRepository carRepository;

    public ModelMapper(UserRepository userRepository,
                       BrandRepository brandRepository,
                       ColorRepository colorRepository,
                       ModelRepository modelRepository,
                       CarRepository carRepository) {
        this.userRepository = userRepository;
        this.brandRepository = brandRepository;
        this.colorRepository = colorRepository;
        this.modelRepository = modelRepository;
        this.carRepository = carRepository;
    }

    public User fromDto(UserDto userDto) {
        User user = new User();
        dtoToObject(user, userDto);
        return user;
    }

    private void dtoToObject(User user, UserDto userDto) {
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setUserCredentials(fromDto(userDto.getUserCredentialsDto()));
    }

    public User fromDto(UserDto userDto, int id) {
        User user = userRepository.getById(id);
        dtoToObject(user, userDto);
        return user;
    }

    public UserCredentials fromDto(UserCredentialsDto userCredentialsDto) {
        UserCredentials userCredentials = new UserCredentials();
        dtoToObject(userCredentials, userCredentialsDto);
        return userCredentials;
    }

    private void dtoToObject(UserCredentials userCredentials, UserCredentialsDto userCredentialsDto) {
        userCredentials.setUsername(userCredentialsDto.getUsername());
    }

    public Car fromDto(CarDto carDto) {
        Car car = new Car();
        dtoToObject(car, carDto);
        return car;
    }

    private void dtoToObject(Car car, CarDto carDto) {
        car.setBrand(brandRepository.getById(carDto.getBrandId()));
        car.setModel(modelRepository.getById(carDto.getModelId()));
        car.setColor(colorRepository.getById(carDto.getColorId()));
        car.setOwner(userRepository.getById(carDto.getUserId()));
        car.setCubature(carDto.getCubature());
        car.setHorsePowers(carDto.getHorsePowers());
        car.setLicencePlate(carDto.getLicencePlate());
    }

    public Car fromDto(CarDto carDto, int id) {
        Car car = carRepository.getById(id);
        dtoToObject(car, carDto);
        return car;
    }

    public CarFilterParameters fromParametersList(Optional<Integer> brandId, Optional<Integer> modelId) {
        return new CarFilterParameters(brandId, modelId);
    }
}
