package com.example.carsdemo.repositories;

import com.example.carsdemo.exceptions.EntityNotFoundException;
import com.example.carsdemo.models.Brand;
import com.example.carsdemo.models.Car;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class BrandRepositoryImpl implements BrandRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public BrandRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Brand> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Brand> query = session.createQuery("from Brand ", Brand.class);
            return query.list();
        }
    }

    @Override
    public Brand getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Brand where id = :id";
            Query<Brand> query = session.createQuery(queryString, Brand.class);
            query.setParameter("id", id);
            List<Brand> brands = query.list();
            if (brands.size() == 0) {
                throw new EntityNotFoundException("Brand", id);
            }
            return brands.get(0);
        }
    }

    @Override
    @Transactional
    public void create(Brand brand) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(brand);
            transaction.commit();
        }
    }

    @Override
    @Transactional
    public void update(Brand brand) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(brand);
            transaction.commit();
        }
    }

    @Override
    @Transactional
    public void delete(Brand brand) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.delete(brand);
            transaction.commit();
        }
    }
}
