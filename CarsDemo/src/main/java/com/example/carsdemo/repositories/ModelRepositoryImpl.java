package com.example.carsdemo.repositories;

import com.example.carsdemo.exceptions.EntityNotFoundException;
import com.example.carsdemo.models.Model;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class ModelRepositoryImpl implements ModelRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public ModelRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Model> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Model> query = session.createQuery("from Model ", Model.class);
            return query.list();
        }
    }

    @Override
    public Model getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Model where id = :id";
            Query<Model> query = session.createQuery(queryString, Model.class);
            query.setParameter("id", id);
            List<Model> brands = query.list();
            if (brands.size() == 0) {
                throw new EntityNotFoundException("Model", id);
            }
            return brands.get(0);
        }
    }

    @Override
    @Transactional
    public void create(Model model) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(model);
            transaction.commit();
        }
    }

    @Override
    @Transactional
    public void update(Model model) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(model);
            transaction.commit();
        }
    }

    @Override
    @Transactional
    public void delete(Model model) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.delete(model);
            transaction.commit();
        }
    }
}
