package com.example.carsdemo.repositories;

import com.example.carsdemo.models.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserRepository {
    List<User> getAll();

    User getById(int id);

    User getByUsername(String username);

    @Transactional
    void create(User user);

    @Transactional
    void update(User user);

    @Transactional
    void delete(User user);
}
