package com.example.carsdemo.repositories;

import com.example.carsdemo.models.Color;

import java.util.List;

public interface ColorRepository {
    List<Color> getAll();

    Color getById(int id);
}
