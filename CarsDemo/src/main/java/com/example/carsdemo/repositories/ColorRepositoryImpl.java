package com.example.carsdemo.repositories;

import com.example.carsdemo.exceptions.EntityNotFoundException;
import com.example.carsdemo.models.Brand;
import com.example.carsdemo.models.Color;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ColorRepositoryImpl implements ColorRepository {

    private final SessionFactory sessionFactory;

    public ColorRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Color> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Color> query = session.createQuery("from Color ", Color.class);
            return query.list();
        }
    }

    @Override
    public Color getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Color where id = :id";
            Query<Color> query = session.createQuery(queryString, Color.class);
            query.setParameter("id", id);
            List<Color> colors = query.list();
            if (colors.size() == 0) {
                throw new EntityNotFoundException("Color", id);
            }
            return colors.get(0);
        }
    }
}
