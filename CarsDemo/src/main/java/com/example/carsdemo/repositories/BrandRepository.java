package com.example.carsdemo.repositories;

import com.example.carsdemo.models.Brand;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface BrandRepository {
    List<Brand> getAll();

    Brand getById(int id);

    @Transactional
    void create(Brand brand);

    @Transactional
    void update(Brand brand);

    @Transactional
    void delete(Brand brand);
}
