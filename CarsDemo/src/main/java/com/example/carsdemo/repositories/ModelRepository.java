package com.example.carsdemo.repositories;

import com.example.carsdemo.models.Model;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ModelRepository {
    List<Model> getAll();

    Model getById(int id);

    @Transactional
    void create(Model model);

    @Transactional
    void update(Model model);

    @Transactional
    void delete(Model model);
}
