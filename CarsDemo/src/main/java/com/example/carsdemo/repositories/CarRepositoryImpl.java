package com.example.carsdemo.repositories;

import com.example.carsdemo.exceptions.EntityNotFoundException;
import com.example.carsdemo.models.Car;
import com.example.carsdemo.models.filterparameters.CarFilterParameters;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CarRepositoryImpl implements CarRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public CarRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<Car> filter(CarFilterParameters cfp) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "from Car where 1=1 ";
            var filters = new ArrayList<String>();

            cfp.getBrandId().ifPresent(q -> filters.add(" and brand.id = :brandId "));
            cfp.getModelId().ifPresent(q -> filters.add(" and model.id = :modelId "));

            queryString = queryString + String.join("", filters);

            Query<Car> query = session.createQuery(queryString, Car.class);
            cfp.getBrandId().ifPresent(first -> query.setParameter("brandId", first));
            cfp.getModelId().ifPresent(last -> query.setParameter("modelId", last));
            return query.list();
        }
    }

    @Override
    public Car getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Car where id = :id";
            Query<Car> query = session.createQuery(queryString, Car.class);
            query.setParameter("id", id);
            List<Car> cars = query.list();
            if (cars.size() == 0) {
                throw new EntityNotFoundException("Car", id);
            }
            return cars.get(0);
        }
    }

    @Override
    public Car getByLicencePlate(String licencePlate) {
        try (Session session = sessionFactory.openSession()) {
            String queryString = "From Car where licencePlate = :licencePlate";
            Query<Car> query = session.createQuery(queryString, Car.class);
            query.setParameter("licencePlate", licencePlate);
            List<Car> cars = query.list();
            if (cars.size() == 0) {
                throw new EntityNotFoundException("Car", "licence plate", licencePlate);
            }
            return cars.get(0);
        }
    }

    @Override
    @Transactional
    public void create(Car car) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.save(car);
            transaction.commit();
        }
    }

    @Override
    @Transactional
    public void update(Car car) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(car);
            transaction.commit();
        }
    }

    @Override
    @Transactional
    public void delete(Car car) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.delete(car);
            transaction.commit();
        }
    }
}
