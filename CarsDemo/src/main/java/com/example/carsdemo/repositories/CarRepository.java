package com.example.carsdemo.repositories;

import com.example.carsdemo.models.Car;
import com.example.carsdemo.models.filterparameters.CarFilterParameters;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CarRepository {

    List<Car> filter(CarFilterParameters cfp);

    Car getById(int id);

    Car getByLicencePlate(String licencePlate);

    @Transactional
    void create(Car car);

    @Transactional
    void update(Car car);

    @Transactional
    void delete(Car car);
}
