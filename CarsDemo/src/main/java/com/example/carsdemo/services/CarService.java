package com.example.carsdemo.services;

import com.example.carsdemo.models.Car;
import com.example.carsdemo.models.filterparameters.CarFilterParameters;

import java.util.List;

public interface CarService {
    List<Car> filter(CarFilterParameters cfp);

    Car getById(int id);

    Car getByLicencePlate(String licencePlate);

    void create(Car car);

    void update(Car car);

    void delete(Car car);
}
