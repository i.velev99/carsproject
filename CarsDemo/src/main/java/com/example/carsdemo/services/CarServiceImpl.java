package com.example.carsdemo.services;

import com.example.carsdemo.exceptions.DuplicateEntityException;
import com.example.carsdemo.exceptions.EntityNotFoundException;
import com.example.carsdemo.models.Car;
import com.example.carsdemo.models.filterparameters.CarFilterParameters;
import com.example.carsdemo.repositories.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;

    @Autowired
    public CarServiceImpl(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Override
    public List<Car> filter(CarFilterParameters cfp) {
        return carRepository.filter(cfp);
    }

    @Override
    public Car getById(int id) {
        return carRepository.getById(id);
    }

    @Override
    public Car getByLicencePlate(String licencePlate) {
        return carRepository.getByLicencePlate(licencePlate);
    }

    @Override
    public void create(Car car) {
        checkDuplicateLicencePlate(car);
        carRepository.create(car);
    }

    @Override
    public void update(Car car) {
        checkDuplicateLicencePlate(car);
        carRepository.update(car);
    }

    @Override
    public void delete(Car car) {
        carRepository.delete(car);
    }

    private void checkDuplicateLicencePlate(Car car) {
        boolean duplicateExists = false;

        try {
            Car result = carRepository.getByLicencePlate(car.getLicencePlate());
            if (car.getId() != result.getId()) {
                duplicateExists = true;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Car", "licence plate", car.getLicencePlate());
        }
    }
}