package com.example.carsdemo.services;

import com.example.carsdemo.models.User;

import java.util.List;

public interface UserService {
    List<User> getAll();

    User getById(int id);

    User getByUsername(String username);

    void create(User user);

    void update(User user);

    void delete(User user);
}
