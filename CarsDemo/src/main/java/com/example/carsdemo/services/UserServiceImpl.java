package com.example.carsdemo.services;

import com.example.carsdemo.exceptions.DuplicateEntityException;
import com.example.carsdemo.exceptions.EntityNotFoundException;
import com.example.carsdemo.models.User;
import com.example.carsdemo.repositories.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService{

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        return userRepository.getAll();
    }

    @Override
    public User getById(int id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByUsername(String username) {
        return userRepository.getByUsername(username);
    }

    @Override
    public void create(User user) {
        checkDuplicateUsername(user);
        userRepository.create(user);
    }

    @Override
    public void update(User user) {
        checkDuplicateUsername(user);
        userRepository.update(user);
    }

    @Override
    public void delete(User user) {
        userRepository.delete(user);
    }

    private void checkDuplicateUsername(User user) {
        boolean duplicateExists = false;

        try {
            User result = userRepository.getByUsername(user.getUserCredentials().getUsername());
            if (user.getId() != result.getId()) {
                duplicateExists = true;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("User", "username", user.getUserCredentials().getUsername());
        }
    }
}
