# CarsProject

CarsProject is web application where you can create cars and users.

1. Open the CarsProject
2. Use CreateDBscript to create the database
3. Fill the database with the other scripts 
Order:
(1. brand script 2. model script 3. color script 4. user credentials script 5. user script 6. cars script)
4. Run the application

Postman collection for testing: https://www.getpostman.com/collections/1a0d4e5ef0ddc2d00b27

### Technologies
- *IntelliJ IDEA* - as a IDE during the whole development process
- *Spring MVC* - as an application framework
- *MariaDB* - as database
- *Hibernate* - access the database
- *Git* - for source control management and documentation / manual of the application
